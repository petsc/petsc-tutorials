
clean:
	rm -rf *.aux *.nav *.vrb *.log *.out *.snm  *.toc *.pyg *.tex *.pdf *~ _minted* slides/*.pdf slides/*.jpg slides/*.bad

newtexfiles:
	git status -u | grep "slides/" | grep -v "\-t\.tex" | grep "\.tex"
